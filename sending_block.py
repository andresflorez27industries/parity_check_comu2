# CREATED BY Andres, Diana and Andres_V

import numpy as np
from gnuradio import gr


class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - a simple multiply const"""


    def __init__(self, maxBit = 10):  # only default arguments here
        """mi bloque es de andresC"""
        gr.sync_block.__init__(
            self,
            name='Parity Check Sending',   # will show up in GRC
            in_sig=[np.int8],
            out_sig=[np.int8]
        )
        # if an attribute with the same name as a parameter is found,
        # a callback is registered (properties work, too).
        self.maxBit = maxBit


    def work(self, input_items, output_items):

	data = input_items[0]
	nBits = len(data)

	if nBits%2 > 0:
		#Odd (Impar)
		np.append(data, 0)
		#data.append(0)
	
	else:
		#Even (Par)    
		#data.append(1)
		np.append(data, 0)
	
        output_items[0][:] = data

        return len(output_items[0])
