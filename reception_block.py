# DEVELOPED BY Andres, Diana and Andres_V

import numpy as np
from gnuradio import gr


class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self, maxBit = 10):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.sync_block.__init__(
            self,
            name='Parity Check Reception',   # will show up in GRC
            in_sig=[np.int8],
            out_sig=[np.int8]
        )
       
        self.maxBit = maxBit

    def work(self, input_items, output_items):

	data = input_items[0]
	data = np.array(data)
	nBits = len(data)

	if nBits%2 > 0:
		#Odd (Impar)
		data = np.delete(data, nBits-1)


        output_items[0][:] = data
        return len(output_items[0]+1)




